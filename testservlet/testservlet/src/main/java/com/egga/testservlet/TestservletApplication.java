package com.egga.testservlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestservletApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestservletApplication.class, args);
	}

}
